Filenames which are not valid:

- contain forward slash `/`
- `.git`
- `.` and `..`, but not `...`
